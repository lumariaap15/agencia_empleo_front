import axios from './GlobalService';

const RegistroService = {
    registroOferente(data) {
        return axios.post('register/oferente', data);
    },
};

export default RegistroService;
