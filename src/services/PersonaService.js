import axios from './GlobalService';

const PersonaService = {
    updatePersona(data) {
        return axios.put('personas/update', data);
    },
    infoAuth() {
        return axios.get('personas/infopersonaauthenticate');
    },
    infoPersona(idPersona) {
        return axios.get('personas/info/'+idPersona);
    },
    perfilLaboral(idPersona){
        return axios.get('perfil_laboral/'+idPersona)
    },
    authPerfilLaboral(){
        return axios.get('perfil_laboral_auth')
    },
    updatePerfil(idPersona, data) {
        return axios.put('perfil_laboral/'+idPersona, data);
    },
};

export default PersonaService;
