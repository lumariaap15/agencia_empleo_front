import axios from 'axios';
import AuthService from "./AuthService";
// axios.defaults.baseURL = 'http://173.230.130.57/api';
axios.defaults.baseURL = 'http://agenciaempleo.test/api';

//interceptamos para que siempre envie el token al post
axios.interceptors.request.use(function(config) {
    const auth_token = localStorage.getItem('token');
    if(auth_token) {
        config.headers.Authorization = `Bearer ${auth_token}`;
    }
    return config;
}, function(err) {
    return Promise.reject(err);
});

//Redireccionamos al login si el token expiró
axios.interceptors.response.use(function (response) {
    return response;
}, function (error) {
    if(error.response.status === 401){
        if(error.response.config.url !== '/api/login'){
            /*
            if (localStorage.getItem("user") !== null){
                AuthService.refresh().then((resp)=>{
                    localStorage.setItem('token', resp.data.access_token);
                    localStorage.setItem('user', JSON.stringify(resp.data.user));
                });
            }
             */
            localStorage.clear();
            window.location.reload()
        }
    }
    return Promise.reject(error);
});

export default axios;
