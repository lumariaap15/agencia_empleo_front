import axios from '../GlobalService';

const DepartamentoService = {
    all() {
        return axios.get('departamentos');
    },
};

export default DepartamentoService;
