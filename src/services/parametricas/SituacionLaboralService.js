import axios from '../GlobalService';
import {store} from "../../store";

const SituacionLaboralService = {
    all() {
        return new Promise(function (resolve, reject) {
            if (store.getters.situacionLaborals.length === 0) {
                let response = axios.get('situacion_laboral');
                response.then((resp) => {
                    resp.data.forEach(item => {
                        store.commit('agregarSituacionLaboral', item);
                    });
                    resolve({data: store.getters.situacionLaborals})
                }).catch(e => {
                    reject(e)
                });
            }else{
                resolve({data: store.getters.situacionLaborals})
            }
        })
    },
    store(data) {
        return axios.post('situacion_laboral', data);
    },
    find(id) {
        return axios.get(`situacion_laboral/${id}`);
    },
    update(id, data) {
        return axios.put(`situacion_laboral/${id}`, data);
    },
    delete(id) {
        store.commit('eliminarSituacionLaboral',id);
        return axios.delete(`situacion_laboral/${id}`);
    },
};

export default SituacionLaboralService;
