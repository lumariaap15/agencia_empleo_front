import axios from '../GlobalService';
import {store} from "../../store";

const IdiomaService = {
    all() {
        return new Promise(function (resolve, reject) {
            if (store.getters.idiomas.length === 0) {
                let response = axios.get('idioma');
                response.then((resp) => {
                    resp.data.forEach(item => {
                        store.commit('agregarIdioma', item);
                    });
                    resolve({data: store.getters.idiomas})
                }).catch(e => {
                    reject(e)
                });
            }else{
                resolve({data: store.getters.idiomas})
            }
        })
    },
    store(data) {
        return axios.post('idioma', data);
    },
    find(id) {
        return axios.get(`idioma/${id}`);
    },
    update(id, data) {
        return axios.put(`idioma/${id}`, data);
    },
    delete(id) {
        store.commit('eliminarIdioma',id);
        return axios.delete(`idioma/${id}`);
    },
};

export default IdiomaService;
