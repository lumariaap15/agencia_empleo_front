import axios from '../GlobalService';
import {store} from "../../store";

const UniversidadService = {
    all() {
        return new Promise(function (resolve, reject) {
            if (store.getters.universidades.length === 0) {
                let response = axios.get('universidad');
                response.then((resp) => {
                    resp.data.forEach(item => {
                        store.commit('agregarUniversidad', item);
                    });
                    resolve({data: store.getters.universidades})
                }).catch(e => {
                    reject(e)
                });
            }else{
                resolve({data: store.getters.universidades})
            }
        })
    },
    store(data) {
        return axios.post('universidad', data);
    },
    find(id) {
        return axios.get(`universidad/${id}`);
    },
    update(id, data) {
        return axios.put(`universidad/${id}`, data);
    },
    delete(id) {
        store.commit('eliminarUniversidad',id);
        return axios.delete(`universidad/${id}`);
    },
};

export default UniversidadService;
