import axios from '../GlobalService';
import {store} from "../../store";

const TipoLicenciaService = {
    all() {
        return new Promise(function (resolve, reject) {
            if (store.getters.tipoLicencias.length === 0) {
                let response = axios.get('tipo_licencia');
                response.then((resp) => {
                    resp.data.forEach(item => {
                        store.commit('agregarTipoLicencia', item);
                    });
                    resolve({data: store.getters.tipoLicencias})
                }).catch(e => {
                    reject(e)
                });
            }else{
                resolve({data: store.getters.tipoLicencias})
            }
        })
    },
    store(data) {
        return axios.post('tipo_licencia', data);
    },
    find(id) {
        return axios.get(`tipo_licencia/${id}`);
    },
    update(id, data) {
        return axios.put(`tipo_licencia/${id}`, data);
    },
    delete(id) {
        store.commit('eliminarTipoLicencia',id);
        return axios.delete(`tipo_licencia/${id}`);
    },
};

export default TipoLicenciaService;
