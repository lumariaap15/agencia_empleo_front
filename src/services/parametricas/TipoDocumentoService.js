import axios from '../GlobalService';
import {store} from "../../store";

const TipoDocumentoService = {
    all(){
        return new Promise(function (resolve, reject) {
            if (store.getters.tiposDeDocumentos.length === 0) {
                let response = axios.get('tipo_documentos');
                response.then((resp) => {
                    resp.data.forEach(item => {
                        store.commit('agregarTipoDocumento', item);
                    });
                    resolve({data: store.getters.tiposDeDocumentos})
                }).catch(e => {
                    reject(e)
                });
            }else{
                resolve({data: store.getters.tiposDeDocumentos})
            }
        })
    },
    store(data) {
        return axios.post('tipo_documentos', data);
    },
    find(id) {
        return axios.get(`tipo_documentos/${id}`);
    },
    update(id, data) {
        //store.commit('actualizarTipoDocumento',{id: id, data: data});
        return axios.put(`tipo_documentos/${id}`, data);
    },
    delete(id) {
        store.commit('eliminarTipoDocumento',id);
        return axios.delete(`tipo_documentos/${id}`);
    },
};

export default TipoDocumentoService;
