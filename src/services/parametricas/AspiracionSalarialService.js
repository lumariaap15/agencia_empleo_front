import axios from '../GlobalService';
import {store} from "../../store";

const AspiracionSalarialService = {
    all() {
        return new Promise(function (resolve, reject) {
            if (store.getters.aspiracionSalarials.length === 0) {
                let response = axios.get('aspiracion_salarial');
                response.then((resp) => {
                    resp.data.forEach(item => {
                        store.commit('agregarAspiracionSalarial', item);
                    });
                    resolve({data: store.getters.aspiracionSalarials})
                }).catch(e => {
                    reject(e)
                });
            }else{
                resolve({data: store.getters.aspiracionSalarials})
            }
        })
    },
    store(data) {
        return axios.post('aspiracion_salarial', data);
    },
    find(id) {
        return axios.get(`aspiracion_salarial/${id}`);
    },
    update(id, data) {
        return axios.put(`aspiracion_salarial/${id}`, data);
    },
    delete(id) {
        store.commit('eliminarAspiracionSalarial',id);
        return axios.delete(`aspiracion_salarial/${id}`);
    },
};

export default AspiracionSalarialService;
