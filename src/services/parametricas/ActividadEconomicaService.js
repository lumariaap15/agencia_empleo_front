import axios from '../GlobalService';
import {store} from "../../store";

const ActividadEconomicaService = {
    all() {
        return new Promise(function (resolve, reject) {
            if (store.getters.actividadEconomicas.length === 0) {
                let response = axios.get('actividad_economica');
                response.then((resp) => {
                    resp.data.forEach(item => {
                        store.commit('agregarActividadEconomica', item);
                    });
                    resolve({data: store.getters.actividadEconomicas})
                }).catch(e => {
                    reject(e)
                });
            }else{
                resolve({data: store.getters.actividadEconomicas})
            }
        })
    },
    store(data) {
        return axios.post('actividad_economica', data);
    },
    find(id) {
        return axios.get(`actividad_economica/${id}`);
    },
    update(id, data) {
        return axios.put(`actividad_economica/${id}`, data);
    },
    delete(id) {
        store.commit('eliminarActividadEconomica',id);
        return axios.delete(`actividad_economica/${id}`);
    },
};

export default ActividadEconomicaService;
