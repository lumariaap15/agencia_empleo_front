import axios from '../GlobalService';
import {store} from "../../store";

const HabilidadService = {
    all() {
        return new Promise(function (resolve, reject) {
            if (store.getters.habilidades.length === 0) {
                let response = axios.get('habilidad');
                response.then((resp) => {
                    resp.data.forEach(item => {
                        store.commit('agregarHabilidad', item);
                    });
                    resolve({data: store.getters.habilidades})
                }).catch(e => {
                    reject(e)
                });
            }else{
                resolve({data: store.getters.habilidades})
            }
        })
    },
    store(data) {
        return axios.post('habilidad', data);
    },
    find(id) {
        return axios.get(`habilidad/${id}`);
    },
    update(id, data) {
        return axios.put(`habilidad/${id}`, data);
    },
    delete(id) {
        store.commit('eliminarHabilidad',id);
        return axios.delete(`habilidad/${id}`);
    },
};

export default HabilidadService;
