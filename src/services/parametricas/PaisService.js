import axios from '../GlobalService';

const PaisService = {
    all() {
        return axios.get('pais');
    },
};

export default PaisService;
