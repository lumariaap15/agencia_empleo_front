import axios from '../GlobalService';
import {store} from "../../store";

const NivelEducativoService = {
    all() {
        return new Promise(function (resolve, reject) {
            if (store.getters.nivelesEducativos.length === 0) {
                let response = axios.get('nivel_educativo');
                response.then((resp) => {
                    resp.data.forEach(item => {
                        store.commit('agregarNivelEducativo', item);
                    });
                    resolve({data: store.getters.nivelesEducativos})
                }).catch(e => {
                    reject(e)
                });
            }else{
                resolve({data: store.getters.nivelesEducativos})
            }
        })
    },
    store(data) {
        return axios.post('nivel_educativo', data);
    },
    find(id) {
        return axios.get(`nivel_educativo/${id}`);
    },
    update(id, data) {
        return axios.put(`nivel_educativo/${id}`, data);
    },
    delete(id) {
        store.commit('eliminarNivelEducativo',id);
        return axios.delete(`nivel_educativo/${id}`);
    },
};

export default NivelEducativoService;
