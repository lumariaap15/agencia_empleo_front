import axios from '../GlobalService';
import {store} from "../../store";

const EducacionNucleoService = {
    all() {
        return new Promise(function (resolve, reject) {
            if (store.getters.educacionNucleos.length === 0) {
                let response = axios.get('educacion_nucleo');
                response.then((resp) => {
                    resp.data.forEach(item => {
                        store.commit('agregarNucleoConocimiento', item);
                    });
                    resolve({data: store.getters.educacionNucleos})
                }).catch(e => {
                    reject(e)
                });
            }else{
                resolve({data: store.getters.educacionNucleos})
            }
        })
    },
    store(data) {
        return axios.post('educacion_nucleo', data);
    },
    find(id) {
        return axios.get(`educacion_nucleo/${id}`);
    },
    update(id, data) {
        return axios.put(`educacion_nucleo/${id}`, data);
    },
    delete(id) {
        store.commit('eliminarNucleoConocimiento',id);
        return axios.delete(`educacion_nucleo/${id}`);
    },
};

export default EducacionNucleoService;
