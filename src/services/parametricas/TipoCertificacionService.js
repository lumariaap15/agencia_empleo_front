import axios from '../GlobalService';
import {store} from "../../store";

const TipoCertificacionService = {
    all() {
        return new Promise(function (resolve, reject) {
            if (store.getters.tipoCertificacions.length === 0) {
                let response = axios.get('tipo_certificacion');
                response.then((resp) => {
                    resp.data.forEach(item => {
                        store.commit('agregarTipoCertificacion', item);
                    });
                    resolve({data: store.getters.tipoCertificacions})
                }).catch(e => {
                    reject(e)
                });
            }else{
                resolve({data: store.getters.tipoCertificacions})
            }
        })
    },
    store(data) {
        return axios.post('tipo_certificacion', data);
    },
    find(id) {
        return axios.get(`tipo_certificacion/${id}`);
    },
    update(id, data) {
        return axios.put(`tipo_certificacion/${id}`, data);
    },
    delete(id) {
        store.commit('eliminarTipoCertificacion',id);
        return axios.delete(`tipo_certificacion/${id}`);
    },
};

export default TipoCertificacionService;
