import axios from '../GlobalService';
import {store} from "../../store";

const SectorEmpresaService = {
    all() {
        return new Promise(function (resolve, reject) {
            if (store.getters.sectorEmpresas.length === 0) {
                let response = axios.get('sector_empresa');
                response.then((resp) => {
                    resp.data.forEach(item => {
                        store.commit('agregarSectorEmpresa', item);
                    });
                    resolve({data: store.getters.sectorEmpresas})
                }).catch(e => {
                    reject(e)
                });
            }else{
                resolve({data: store.getters.sectorEmpresas})
            }
        })
    },
    store(data) {
        return axios.post('sector_empresa', data);
    },
    find(id) {
        return axios.get(`sector_empresa/${id}`);
    },
    update(id, data) {
        return axios.put(`sector_empresa/${id}`, data);
    },
    delete(id) {
        store.commit('eliminarSectorEmpresa',id);
        return axios.delete(`sector_empresa/${id}`);
    },
};

export default SectorEmpresaService;
