import axios from '../GlobalService';
import {store} from "../../store";

const EstadoCivilService = {
    all() {
        return new Promise(function (resolve, reject) {
            if (store.getters.estadosCiviles.length === 0) {
                let response = axios.get('estado_civiles');
                response.then((resp) => {
                    resp.data.forEach(item => {
                        store.commit('agregarEstadoCivil', item);
                    });
                    resolve({data: store.getters.estadosCiviles})
                }).catch(e => {
                    reject(e)
                });
            }else{
                resolve({data: store.getters.estadosCiviles})
            }
        })
    },
    store(data) {
        return axios.post('estado_civiles', data);
    },
    find(id) {
        return axios.get(`estado_civiles/${id}`);
    },
    update(id, data) {
        //store.commit('actualizarEstadoCivil',{id: id, data: data});
        return axios.put(`estado_civiles/${id}`, data);
    },
    delete(id) {
        store.commit('eliminarEstadoCivil',id);
        return axios.delete(`estado_civiles/${id}`);
    },
};

export default EstadoCivilService;
