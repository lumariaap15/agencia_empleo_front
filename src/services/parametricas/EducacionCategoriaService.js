import axios from '../GlobalService';

const EducacionCategoriaService = {
    all(query) {
        return axios.get('educacion_categoria'+query)
    },
    store(data) {
        return axios.post('educacion_categoria', data);
    },
    find(id) {
        return axios.get(`educacion_categoria/${id}`);
    },
    update(id, data) {
        return axios.put(`educacion_categoria/${id}`, data);
    },
    delete(id) {
        return axios.delete(`educacion_categoria/${id}`);
    },
};

export default EducacionCategoriaService;
