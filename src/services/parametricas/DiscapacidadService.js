import axios from '../GlobalService';
import {store} from "../../store";

const DiscapacidadService = {
    all() {
        return new Promise(function (resolve, reject) {
            if (store.getters.discapaciads.length === 0) {
                let response = axios.get('tipo_discapacidad');
                response.then((resp) => {
                    resp.data.forEach(item => {
                        store.commit('agregarDiscapacidad', item);
                    });
                    resolve({data: store.getters.discapaciads})
                }).catch(e => {
                    reject(e)
                });
            }else{
                resolve({data: store.getters.discapaciads})
            }
        })
    },
    store(data) {
        return axios.post('tipo_discapacidad', data);
    },
    find(id) {
        return axios.get(`tipo_discapacidad/${id}`);
    },
    update(id, data) {
        //store.commit('actualizarEstadoCivil',{id: id, data: data});
        return axios.put(`tipo_discapacidad/${id}`, data);
    },
    delete(id) {
        store.commit('eliminarDiscapacidad',id);
        return axios.delete(`tipo_discapacidad/${id}`);
    },
};

export default DiscapacidadService;
