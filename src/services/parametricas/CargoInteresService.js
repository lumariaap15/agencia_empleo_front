import axios from '../GlobalService';
import {store} from "../../store";

const CargoInteresService = {
    all() {
        return new Promise(function (resolve, reject) {
            if (store.getters.cargosInteres.length === 0) {
                let response = axios.get('cargo_interes');
                response.then((resp) => {
                    resp.data.forEach(item => {
                        store.commit('agregarCargo', item);
                    });
                    resolve({data: store.getters.cargosInteres})
                }).catch(e => {
                    reject(e)
                });
            }else{
                resolve({data: store.getters.cargosInteres})
            }
        })
    },
    store(data) {
        return axios.post('cargo_interes', data);
    },
    find(id) {
        return axios.get(`cargo_interes/${id}`);
    },
    update(id, data) {
        return axios.put(`cargo_interes/${id}`, data);
    },
    delete(id) {
        store.commit('eliminarCargo',id);
        return axios.delete(`cargo_interes/${id}`);
    },
};

export default CargoInteresService;
