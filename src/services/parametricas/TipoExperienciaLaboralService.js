import axios from '../GlobalService';
import {store} from "../../store";

const TipoExperienciaLaboralService = {
    all() {
        return new Promise(function (resolve, reject) {
            if (store.getters.tipoExperienciaLaborals.length === 0) {
                let response = axios.get('tipo_experiencia_laboral');
                response.then((resp) => {
                    resp.data.forEach(item => {
                        store.commit('agregarTipoExperienciaLaboral', item);
                    });
                    resolve({data: store.getters.tipoExperienciaLaborals})
                }).catch(e => {
                    reject(e)
                });
            }else{
                resolve({data: store.getters.tipoExperienciaLaborals})
            }
        })
    },
    store(data) {
        return axios.post('tipo_experiencia_laboral', data);
    },
    find(id) {
        return axios.get(`tipo_experiencia_laboral/${id}`);
    },
    update(id, data) {
        return axios.put(`tipo_experiencia_laboral/${id}`, data);
    },
    delete(id) {
        store.commit('eliminarTipoExperienciaLaboral',id);
        return axios.delete(`tipo_experiencia_laboral/${id}`);
    },
};

export default TipoExperienciaLaboralService;
