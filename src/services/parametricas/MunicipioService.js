import axios from '../GlobalService';

const MunicipioService = {
    all(departamentoCodigo) {
        return axios.get('municipios/'+departamentoCodigo);
    },
};

export default MunicipioService;
