import axios from '../GlobalService';
import {store} from "../../store";

const PreguntaSeguridadService = {
    all(){
        return new Promise(function (resolve, reject) {
            if (store.getters.preguntas.length === 0) {
                let response = axios.get('user_pregunta');
                response.then((resp) => {
                    resp.data.forEach(item => {
                        store.commit('agregarPregunta', item);
                    });
                    resolve({data: store.getters.preguntas})
                }).catch(e => {
                    reject(e)
                });
            }else{
                resolve({data: store.getters.preguntas})
            }
        })
    },
    store(data) {
        return axios.post('user_pregunta', data);
    },
    find(id) {
        return axios.get(`user_pregunta/${id}`);
    },
    update(id, data) {
        //store.commit('actualizarTipoDocumento',{id: id, data: data});
        return axios.put(`user_pregunta/${id}`, data);
    },
    delete(id) {
        store.commit('eliminarPreguntaTipoDocumento',id);
        return axios.delete(`user_pregunta/${id}`);
    },
};

export default PreguntaSeguridadService;
