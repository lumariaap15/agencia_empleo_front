import axios from '../GlobalService';
import {store} from "../../store";

const TipoEmpresaService = {
    all() {
        return new Promise(function (resolve, reject) {
            if (store.getters.tipoEmpresas.length === 0) {
                let response = axios.get('tipo_empresa');
                response.then((resp) => {
                    resp.data.forEach(item => {
                        store.commit('agregarTipoEmpresa', item);
                    });
                    resolve({data: store.getters.tipoEmpresas})
                }).catch(e => {
                    reject(e)
                });
            }else{
                resolve({data: store.getters.tipoEmpresas})
            }
        })
    },
    store(data) {
        return axios.post('tipo_empresa', data);
    },
    find(id) {
        return axios.get(`tipo_empresa/${id}`);
    },
    update(id, data) {
        return axios.put(`tipo_empresa/${id}`, data);
    },
    delete(id) {
        store.commit('eliminarTipoEmpresa',id);
        return axios.delete(`tipo_empresa/${id}`);
    },
};

export default TipoEmpresaService;