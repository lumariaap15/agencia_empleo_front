import axios from '../GlobalService';
import {store} from "../../store";

const GrupoEtnicoService = {
    all() {
        return new Promise(function (resolve, reject) {
            if (store.getters.gruposEtnicos.length === 0) {
                let response = axios.get('grupo_etnicos');
                response.then((resp) => {
                    resp.data.forEach(item => {
                        store.commit('agregarGrupoEtnico', item);
                    });
                    resolve({data: store.getters.gruposEtnicos})
                }).catch(e => {
                    reject(e)
                });
            }else{
                resolve({data: store.getters.gruposEtnicos})
            }
        })
    },
    store(data) {
        return axios.post('grupo_etnicos', data);
    },
    find(id) {
        return axios.get(`grupo_etnicos/${id}`);
    },
    update(id, data) {
        //store.commit('actualizarGrupoEtnico',{id: id, data: data});
        return axios.put(`grupo_etnicos/${id}`, data);
    },
    delete(id) {
        store.commit('eliminarGrupoEtnico',id);
        return axios.delete(`grupo_etnicos/${id}`);
    },
};

export default GrupoEtnicoService;
