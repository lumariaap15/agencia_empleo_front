import axios from './GlobalService';

const AuthService = {
    loginAdmin(data) {
        return axios.post('auth/login', data);
    },
    loginOferente(data) {
        return axios.post('auth/loginoferente', data);
    },
    loginDemandante(data) {
        return axios.post('auth/logindemandante', data);
    },
    me() {
        return axios.post('auth/me');
    },
    logout() {
        return axios.post('auth/logout');
    },
    refresh() {
        return axios.post('auth/refresh');
    },
};

export default AuthService;
