import axios from '../GlobalService';

const PersonaIdiomaService = {
    all(personaId) {
        return axios.get('persona_idioma?persona='+personaId);
    },
    store(data) {
        return axios.post('persona_idioma',data);
    },
    show(id) {
        return axios.get('persona_idioma/'+id);
    },
    update(id, data) {
        return axios.put('persona_idioma/'+id,data);
    },
    delete(id) {
        return axios.delete('persona_idioma/'+id);
    },
};

export default PersonaIdiomaService;