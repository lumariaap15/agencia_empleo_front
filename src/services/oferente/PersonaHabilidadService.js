import axios from '../GlobalService';

const PersonaHabilidadService = {
    all(personaId) {
        return axios.get('persona_habilidad?persona='+personaId);
    },
    store(data) {
        return axios.post('persona_habilidad',data);
    },
    show(id) {
        return axios.get('persona_habilidad/'+id);
    },
    update(id, data) {
        return axios.put('persona_habilidad/'+id,data);
    },
    delete(id) {
        return axios.delete('persona_habilidad/'+id);
    },
};

export default PersonaHabilidadService;