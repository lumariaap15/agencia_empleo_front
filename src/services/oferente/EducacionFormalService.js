import axios from '../GlobalService';

const EducacionFormalService = {
    all(personaId) {
        return axios.get('persona_educacion?persona='+personaId);
    },
    store(data) {
        return axios.post('persona_educacion',data);
    },
    show(id) {
        return axios.get('persona_educacion/'+id);
    },
    update(id, data) {
        return axios.put('persona_educacion/'+id,data);
    },
    delete(id) {
        return axios.delete('persona_educacion/'+id);
    },
};

export default EducacionFormalService;