import axios from '../GlobalService';

const ExperienciaLaboralService = {
    all() {
        return axios.get('experiencia-laborals');
    },
    store(data) {
        return axios.post('experiencia-laborals',data);
    },
    update(id, data) {
        return axios.put('experiencia-laborals/'+id,data);
    },
};

export default ExperienciaLaboralService;