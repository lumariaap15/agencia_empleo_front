import axios from '../GlobalService';

const EducacionInformalService = {
    all(personaId) {
        return axios.get('persona_certificacion?persona='+personaId);
    },
    store(data) {
        return axios.post('persona_certificacion',data);
    },
    show(id) {
        return axios.get('persona_certificacion/'+id);
    },
    update(id, data) {
        return axios.put('persona_certificacion/'+id,data);
    },
    delete(id) {
        return axios.delete('persona_certificacion/'+id);
    },
};

export default EducacionInformalService;