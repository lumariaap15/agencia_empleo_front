import Vue from 'vue'
import VueRouter from 'vue-router'
import inicio from "../views/inicio/inicio";
import admin from "../views/panel/layout/admin";
import perfil_personas from "../views/panel/oferente/perfil_personas";
import hoja_vida from "../views/panel/oferente/hoja_vida/hoja_vida";
import procesos from "../views/panel/oferente/procesos";
import ruta_empleo from "../views/panel/oferente/ruta_empleo";
import historico from "../views/panel/oferente/historico";
import registroDemandante from "../views/panel/demandante/registro_demandante.vue";
import demandante from "../views/panel/demandante/demandante.vue";
import RegistroBuscadorForm from "../views/registro/RegistroBuscadorForm";

Vue.use(VueRouter);

const routes = [
    //login
    {
        path: '/',
        name: 'inicio',
        component: inicio
    },
    {
        path: '/personas/registro',
        name: 'personas.registro',
        component: RegistroBuscadorForm
    },
    {
        path: '/prueba',
        name: 'prueba',
        component: registroDemandante
    },
    //RUTAS OFERENTE
    {
        path: '/panel',
        component: admin,
        children:[
            {
                path: '/',
                component: perfil_personas,
                children:[
                    {
                        path: '/',
                        redirect: '/personas/hoja_vida'
                    },
                    {
                        path: '/personas/hoja_vida',
                        name: 'hoja_vida',
                        component: hoja_vida
                    },
                    {
                        path: '/personas/procesos',
                        name: 'procesos',
                        component: procesos
                    },
                    {
                        path: '/personas/ruta_empleo',
                        name: 'ruta_empleo',
                        component: ruta_empleo
                    },
                    {
                        path: '/personas/historico',
                        name: 'historico',
                        component: historico
                    },
                ]
            },
            {
                path:'demandantes',
                component: demandante,
                children:[
                    // {
                    //     path: '/',
                    //     redirect: 'registro'
                    // },
                    {
                        path: 'registro',
                        component: registroDemandante
                    },
                ]
            }
        ]
    }
    // { path: '/panel', name: 'panel', component: () => import( '../views/panel/admin.vue') }
    // { path: '/panel', name: 'panel', component: () => import( '../views/panel/admin.vue') }
]

const router = new VueRouter({
    mode: 'history',
    routes
})

router.beforeEach((to, from, next) => {
    const user = JSON.parse(localStorage.getItem('user'));
    let ignoreRoutes = [
        "/personas/registro",
        "/",
        "/panel/demandantes/registro",
        "/prueba",
        "/panel",
        "/panel/demandantes"
    ];

    if(!ignoreRoutes.includes(to.path)) {
        if (user === null) {
            next({name: 'inicio'});
        } else {
            if(to.meta.roles !== undefined) {
                if (to.meta.roles.includes(user.role)) {
                    next();
                } else {
                    console.log('sin permisos')
                    //next({name: 'sinpermisos'})
                }
            }else{
                next();
            }
        }
    }else{
        console.log(to)
        next();
        /*
        if(user !== null){
            if(to.meta.roles !== undefined) {
                if (to.meta.roles.includes(user.role)) {
                    next({name: 'hoja_vida'});
                } else {
                    console.log('sin permisos')
                    //next({name: 'sinpermisos'})
                }
            }else{
                next({name: 'hoja_vida'})
            }
        }else{
            console.log('entreeeeee')
            next()
        }

         */
    }
});

export default router
