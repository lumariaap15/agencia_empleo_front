import Vue from 'vue';
import App from './App.vue';

import VueSwal from 'vue-swal'
import router from './router'
import {store} from './store'

import { ValidationProvider, extend, ValidationObserver } from 'vee-validate';
import { BootstrapVue } from 'bootstrap-vue'

// Install BootstrapVue
Vue.use(BootstrapVue)
// Register it globally
Vue.component('ValidationProvider', ValidationProvider);
Vue.component('ValidationObserver', ValidationObserver);
import vSelect from 'vue-select'

Vue.component('v-select', vSelect)

// Import component
import Loading from 'vue-loading-overlay';
// Import stylesheet
import 'vue-loading-overlay/dist/vue-loading.css';
// Init plugin
Vue.component('Loading',Loading)

Vue.use(VueSwal)

import * as rules from 'vee-validate/dist/rules';
import es from 'vee-validate/dist/locale/es';

// loop over all rules
for (let rule in rules) {
  extend(rule, {
    ...rules[rule], // add the rule
    message: es.messages[rule] // add its message
  });
}

Vue.config.productionTip = false;


new Vue({
  render: h => h(App),
  router,
  store,
}).$mount('#app')
