const iuState = {
    state: {
        menuHome: true
    },
    mutations: {
       onMenuHome(state){ state.menuHome = true }, 
       offMenuHome(state){ state.menuHome = false } 
    },
    actions: {
    },
    getters:{
        verMenu:(state)=>  state.menuHome
    }
}
export default iuState;