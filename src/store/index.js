import Vue from 'vue'
import Vuex from 'vuex'
import iuState from './ui_state'
import AuthModule from './modules/AuthModule';

Vue.use(Vuex);

export const store =  new Vuex.Store({
    state: {
        textInfo: `Dirección: Calle 10 No. 10-60 Barrio Centro.- Palacio Arnaldo Riobueno Riveros. Puerto Gaitán. JORGE PLAZAS HERNÁNDEZ, Alcalde Municipal 2020-2023. "A paso Firme". Visítenos!`,
        tiposDeDocumentos:[],
        estadosCiviles:[],
        gruposEtnicos:[],
        preguntas:[],
        discapacidades:[],
        aspiracionSalarials:[],
        cargosInteres:[],
        situacionLaborals:[],
        tipoLicencias:[],
        tipoEmpresas:[],
        educacionNucleos:[],
        educacionCategorias:[],
        nivelesEducativos:[],
        universidades:[],
        tipoCertificacions:[],
        idiomas:[],
        habilidades:[],
        sectorEmpresas:[],
        tipoExperienciaLaborals:[],
        actividadEconomicas:[]
    },
    mutations: {
        //TIPOS DE DOCUMENTOS
        agregarTipoDocumento(state, tipoDocumento){
            state.tiposDeDocumentos = [...state.tiposDeDocumentos, tipoDocumento]
        },
        actualizarTipoDocumento(state, tipoDocumento){
            let tipos = state.tiposDeDocumentos;
            tipos = tipos.map(item => {
                if(item.id === tipoDocumento.id){
                    return tipoDocumento.data
                }else{
                    return item
                }
            });
            state.tiposDeDocumentos = tipos
        },
        eliminarTipoDocumento(state, id){
            let tipos = state.tiposDeDocumentos;
            tipos = tipos.filter(item => {
                if(item.id !== id){
                    return item
                }
            });
            state.tiposDeDocumentos = tipos
        },
        //PREGUNTAS DE SEGURIDAD
        agregarPregunta(state, pregunta){
            state.preguntas = [...state.preguntas, pregunta]
        },
        actualizarPregunta(state, pregunta){
            let tipos = state.preguntas;
            tipos = tipos.map(item => {
                if(item.id === pregunta.id){
                    return pregunta.data
                }else{
                    return item
                }
            });
            state.preguntas = tipos
        },
        eliminarPregunta(state, id){
            let tipos = state.preguntas;
            tipos = tipos.filter(item => {
                if(item.id !== id){
                    return item
                }
            });
            state.preguntas = tipos
        },
        //ESTADOS CIVILES
        agregarEstadoCivil(state, item){
            state.estadosCiviles = [...state.estadosCiviles, item]
        },
        actualizarEstadoCivil(state, estadoCivil){
            let items = state.estadosCiviles;
            items = items.map(item => {
                if(item.id === estadoCivil.id){
                    return estadoCivil.data
                }else{
                    return item
                }
            });
            state.estadosCiviles = items
        },
        eliminarEstadoCivil(state, id){
            let items = state.estadosCiviles;
            items = items.filter(item => {
                if(item.id !== id){
                    return item
                }
            });
            state.estadosCiviles = items
        },
        //GRUPOS ÉTNICOS
        agregarGrupoEtnico(state, item){
            state.gruposEtnicos = [...state.gruposEtnicos, item]
        },
        actualizarGrupoEtnico(state, grupo){
            let items = state.gruposEtnicos;
            items = items.map(item => {
                if(item.id === grupo.id){
                    return grupo.data
                }else{
                    return item
                }
            });
            state.gruposEtnicos = items
        },
        eliminarGrupoEtnico(state, id){
            let items = state.gruposEtnicos;
            items = items.filter(item => {
                if(item.id !== id){
                    return item
                }
            });
            state.gruposEtnicos = items
        },
        //DISCAPACIDADES
        agregarDiscapacidad(state, item){
            state.discapacidades = [...state.discapacidades, item]
        },
        actualizarDiscapacidad(state, grupo){
            let items = state.discapacidades;
            items = items.map(item => {
                if(item.id === grupo.id){
                    return grupo.data
                }else{
                    return item
                }
            });
            state.discapacidades = items
        },
        eliminarDiscapacidad(state, id){
            let items = state.discapacidades;
            items = items.filter(item => {
                if(item.id !== id){
                    return item
                }
            });
            state.discapacidades = items
        },
        //CARGOS INTERÉS
        agregarCargo(state, item){
            state.cargosInteres = [...state.cargosInteres, item]
        },
        actualizarCargo(state, grupo){
            let items = state.cargosInteres;
            items = items.map(item => {
                if(item.id === grupo.id){
                    return grupo.data
                }else{
                    return item
                }
            });
            state.cargosInteres = items
        },
        eliminarCargo(state, id){
            let items = state.cargosInteres;
            items = items.filter(item => {
                if(item.id !== id){
                    return item
                }
            });
            state.cargosInteres = items
        },
        //ASPIRACIÓN SALARIAL
        agregarAspiracionSalarial(state, item){
            state.aspiracionSalarials = [...state.aspiracionSalarials, item]
        },
        actualizarAspiracionSalarial(state, grupo){
            let items = state.aspiracionSalarials;
            items = items.map(item => {
                if(item.id === grupo.id){
                    return grupo.data
                }else{
                    return item
                }
            });
            state.aspiracionSalarials = items
        },
        eliminarAspiracionSalarial(state, id){
            let items = state.aspiracionSalarials;
            items = items.filter(item => {
                if(item.id !== id){
                    return item
                }
            });
            state.aspiracionSalarials = items
        },
        //SITUACIÓN LABORAL
        agregarSituacionLaboral(state, item){
            state.situacionLaborals = [...state.situacionLaborals, item]
        },
        actualizarSituacionLaboral(state, grupo){
            let items = state.situacionLaborals;
            items = items.map(item => {
                if(item.id === grupo.id){
                    return grupo.data
                }else{
                    return item
                }
            });
            state.situacionLaborals = items
        },
        eliminarSituacionLaboral(state, id){
            let items = state.situacionLaborals;
            items = items.filter(item => {
                if(item.id !== id){
                    return item
                }
            });
            state.situacionLaborals = items
        },
        //TIPO LICENCIA
        agregarTipoLicencia(state, item){
            state.tipoLicencias = [...state.tipoLicencias, item]
        },
        actualizarTipoLicencia(state, grupo){
            let items = state.tipoLicencias;
            items = items.map(item => {
                if(item.id === grupo.id){
                    return grupo.data
                }else{
                    return item
                }
            });
            state.tipoLicencias = items
        },
        eliminarTipoLicencia(state, id){
            let items = state.tipoLicencias;
            items = items.filter(item => {
                if(item.id !== id){
                    return item
                }
            });
            state.tipoLicencias = items
        },
        //TIPO EMPRESA
        agregarTipoEmpresa(state, item){
            state.tipoEmpresas = [...state.tipoEmpresas, item]
        },
        actualizarTipoEmpresa(state, grupo){
            let items = state.tipoEmpresas;
            items = items.map(item => {
                if(item.id === grupo.id){
                    return grupo.data
                }else{
                    return item
                }
            });
            state.tipoEmpresas = items
        },
        eliminarTipoEmpresa(state, id){
            let items = state.tipoEmpresas;
            items = items.filter(item => {
                if(item.id !== id){
                    return item
                }
            });
            state.tipoEmpresas = items
        },
        //EDUCACIÓN NÚCLEOS
        agregarNucleoConocimiento(state, item){
            state.educacionNucleos = [...state.educacionNucleos, item]
        },
        actualizarNucleoConocimiento(state, grupo){
            let items = state.educacionNucleos;
            items = items.map(item => {
                if(item.id === grupo.id){
                    return grupo.data
                }else{
                    return item
                }
            });
            state.educacionNucleos = items
        },
        eliminarNucleoConocimiento(state, id){
            let items = state.educacionNucleos;
            items = items.filter(item => {
                if(item.id !== id){
                    return item
                }
            });
            state.educacionNucleos = items
        },
        //NIVELES EDUCATIVOS
        agregarNivelEducativo(state, item){
            state.nivelesEducativos = [...state.nivelesEducativos, item]
        },
        actualizarNivelEducativo(state, grupo){
            let items = state.nivelesEducativos;
            items = items.map(item => {
                if(item.id === grupo.id){
                    return grupo.data
                }else{
                    return item
                }
            });
            state.nivelesEducativos = items
        },
        eliminarNivelEducativo(state, id){
            let items = state.nivelesEducativos;
            items = items.filter(item => {
                if(item.id !== id){
                    return item
                }
            });
            state.nivelesEducativos = items
        },
        //UNIVERSIDADES
        agregarUniversidad(state, item){
            state.universidades = [...state.universidades, item]
        },
        actualizarUniversidad(state, grupo){
            let items = state.universidades;
            items = items.map(item => {
                if(item.id === grupo.id){
                    return grupo.data
                }else{
                    return item
                }
            });
            state.universidades = items
        },
        eliminarUniversidad(state, id){
            let items = state.universidades;
            items = items.filter(item => {
                if(item.id !== id){
                    return item
                }
            });
            state.universidades = items
        },
        //Tipo certificacion
        agregarTipoCertificacion(state, item){
            state.tipoCertificacions = [...state.tipoCertificacions, item]
        },
        actualizarTipoCertificacion(state, grupo){
            let items = state.tipoCertificacions;
            items = items.map(item => {
                if(item.id === grupo.id){
                    return grupo.data
                }else{
                    return item
                }
            });
            state.tipoCertificacions = items
        },
        eliminarTipoCertificacion(state, id){
            let items = state.tipoCertificacions;
            items = items.filter(item => {
                if(item.id !== id){
                    return item
                }
            });
            state.tipoCertificacions = items
        },
        //idiomas
        agregarIdioma(state, item){
            state.idiomas = [...state.idiomas, item]
        },
        actualizarIdioma(state, grupo){
            let items = state.idiomas;
            items = items.map(item => {
                if(item.id === grupo.id){
                    return grupo.data
                }else{
                    return item
                }
            });
            state.idiomas = items
        },
        eliminarIdioma(state, id){
            let items = state.idiomas;
            items = items.filter(item => {
                if(item.id !== id){
                    return item
                }
            });
            state.idiomas = items
        },
        //habilidades
        agregarHabilidad(state, item){
            state.habilidades = [...state.habilidades, item]
        },
        actualizarHabilidad(state, grupo){
            let items = state.habilidades;
            items = items.map(item => {
                if(item.id === grupo.id){
                    return grupo.data
                }else{
                    return item
                }
            });
            state.habilidades = items
        },
        eliminarHabilidad(state, id){
            let items = state.habilidades;
            items = items.filter(item => {
                if(item.id !== id){
                    return item
                }
            });
            state.habilidades = items
        },
        //sectorEmpresa
        agregarSectorEmpresa(state, item){
            state.sectorEmpresas = [...state.sectorEmpresas, item]
        },
        actualizarSectorEmpresa(state, grupo){
            let items = state.sectorEmpresas;
            items = items.map(item => {
                if(item.id === grupo.id){
                    return grupo.data
                }else{
                    return item
                }
            });
            state.sectorEmpresas = items
        },
        eliminarSectorEmpresa(state, id){
            let items = state.sectorEmpresas;
            items = items.filter(item => {
                if(item.id !== id){
                    return item
                }
            });
            state.sectorEmpresas = items
        },
        //tipoExperienciaLaboral
        agregarTipoExperienciaLaboral(state, item){
            state.tipoExperienciaLaborals = [...state.tipoExperienciaLaborals, item]
        },
        actualizarTipoExperienciaLaboral(state, grupo){
            let items = state.tipoExperienciaLaborals;
            items = items.map(item => {
                if(item.id === grupo.id){
                    return grupo.data
                }else{
                    return item
                }
            });
            state.tipoExperienciaLaborals = items
        },
        eliminarTipoExperienciaLaboral(state, id){
            let items = state.tipoExperienciaLaborals;
            items = items.filter(item => {
                if(item.id !== id){
                    return item
                }
            });
            state.tipoExperienciaLaborals = items
        },
        //actividadEconomica
        agregarActividadEconomica(state, item){
            state.actividadEconomicas = [...state.actividadEconomicas, item]
        },
        actualizarActividadEconomica(state, grupo){
            let items = state.actividadEconomicas;
            items = items.map(item => {
                if(item.id === grupo.id){
                    return grupo.data
                }else{
                    return item
                }
            });
            state.actividadEconomicas = items
        },
        eliminarActividadEconomica(state, id){
            let items = state.actividadEconomicas;
            items = items.filter(item => {
                if(item.id !== id){
                    return item
                }
            });
            state.actividadEconomicas = items
        },
    },
    actions: {
    },
    modules: {
        iuState,
        AuthModule
    },
    getters:{
        tiposDeDocumentos: state => state.tiposDeDocumentos,
        gruposEtnicos: state => state.gruposEtnicos,
        estadosCiviles: state => state.estadosCiviles,
        preguntas: state => state.preguntas,
        discapaciads: state => state.discapacidades,
        aspiracionSalarials: state => state.aspiracionSalarials,
        cargosInteres: state => state.cargosInteres,
        situacionLaborals: state => state.situacionLaborals,
        tipoLicencias: state => state.tipoLicencias,
        tipoEmpresas: state => state.tipoEmpresas,
        universidades: state => state.universidades,
        nivelesEducativos: state => state.nivelesEducativos,
        educacionNucleos: state => state.educacionNucleos,
        tipoCertificacions: state => state.tipoCertificacions,
        habilidades: state => state.habilidades,
        idiomas: state => state.idiomas,
        tipoExperienciaLaborals: state => state.tipoExperienciaLaborals,
        sectorEmpresas: state => state.sectorEmpresas,
        actividadEconomicas: state => state.actividadEconomicas,
    }
})
