import PersonaService from "../../services/PersonaService";


const AuthModule =  {
    state: {
        authPersona: {
            id: "",
            tipo_documento_id: "",
            tipo_documento: "",
            numero_documento: "",
            primer_nombre: "",
            segundo_nombre: "",
            primer_apellido: "",
            segundo_apellido: "",
            fecha_nacimiento: "",
            estado_civil_id: "",
            estado_civil: "",
            sexo: "",
            tipo_libreta: "",
            numero_libreta: "",
            pais_nacimiento_id: "",
            departamento_nacimiento_codigo: "",
            municipio_nacimiento_codigo: "",
            nacionalidad_id: "",
            jefe_hogar: "",
            grupo_etnico_id: "",
            grupo_etnico: "",
            email: "",
            telefono_uno: "",
            telefono_dos: "",
            observacion: "",
            pais_residencia_id: "",
            departamento_residencia_codigo: "",
            municipio_residencia_codigo: "",
            departamento_residencia: "",
            municipio_residencia: "",
            direccion: "",
            complemento_direccion: "",
            tipo_direccion: "",
            codigo_postal: "",
            user_id: "",
            created_at: "",
            updated_at: "",
            data_update: "",
            discapaciads: "",
            experiencia_laboral: "",
        },
        authPerfilLaboral:{
            id: "",
            persona_id: "",
            perfil_laboral: "",
            aspiracion_salarial_id: "",
            posibilidad_viajar: 0,
            posibilidad_trasladarse: 0,
            ofertas_teletrabajo: 0,
            situacion_laboral_id: "",
            propiedad_transporte: 0,
            tipo_licencia_conduccion_carro_id: "",
            tipo_licencia_conduccion_moto_id: "",
            created_at: "",
            updated_at: "",
            cargos_interes: [],
            aspiracion_salarial: "",
            situacion_laboral: "",
            tipo_licencia_conduccion_carro: "",
            tipo_licencia_conduccion_moto: "",
        },
    },
    actions: {
        getInfoPersona(context) {
            return new Promise(function (resolve, reject) {
                PersonaService.infoAuth().then((persona) => {
                    context.commit('setPersona',persona.data)
                    resolve(context.state.authPersona)
                }).catch(e => {
                    reject(e)
                });
            })

        },
        getPerfilLaboral(context) {
            return new Promise(function (resolve, reject) {
                PersonaService.authPerfilLaboral().then((perfil) => {
                    context.commit('setPerfilLaboral',perfil.data)
                    resolve(context.state.authPerfilLaboral)
                }).catch(e => {
                    reject(e)
                });
            })

        }
    },
    mutations: {
        setPersona(state, persona){
            state.authPersona = persona
        },
        setPerfilLaboral(state, perfil){
            state.authPerfilLaboral = perfil
        },
    },
    getters:{
        authNombreCompleto:(state)=>{
            let segundoNombre = state.authPersona.segundo_nombre !== null ? state.authPersona.segundo_nombre+' ' : ''
            let segundoApellido = state.authPersona.segundo_apellido !== null ? state.authPersona.segundo_apellido+' ' : ''
            return state.authPersona.primer_nombre+' '+segundoNombre
                +state.authPersona.primer_apellido+' '+segundoApellido
        },
        authResidencia:(state)=>{
            return state.authPersona.departamento_residencia_codigo !== null ? (state.authPersona.departamento_residencia.descripcion+', '+
                state.authPersona.municipio_residencia.descripcion):state.authPersona.pais_residencia.descripcion;
        },
        authTipoDocumento:(state)=>{
            return  (state.authPersona.tipo_documento_id !== null && state.authPersona.tipo_documento_id !== "") ? state.authPersona.tipo_documento.descripcion : 'Documento de identidad:';
        },
        authNacionalidad:(state)=>{
            return  (state.authPersona.nacionalidad_id !== null && state.authPersona.nacionalidad_id !== "") ? state.authPersona.nacionalidad.descripcion : '-';
        },
        authNacimiento:(state)=>{
            let resp = "";
            if(state.authPersona.departamento_residencia_codigo !== null){
                resp = state.authPersona.departamento_residencia.descripcion+', '+
                    state.authPersona.municipio_residencia.descripcion;
            }else if(state.authPersona.pais_residencia !== null){
                resp = state.authPersona.pais_residencia.descripcion
            }else{
                resp = "-"
            }
            return resp;
        },
        authEstadoCivil:(state)=>{
            return state.authPersona.estado_civil_id !== null ? state.authPersona.estado_civil.descripcion : '-';
        },
        authDiscapacidades:(state)=>{
            let discapacidadsStr = '';
            let discapacidads = state.authPersona.discapaciads;
            for(let i = 0; i<discapacidads.length; i++){
                discapacidadsStr+=discapacidads[i].descripcion;
                if(i !== (discapacidads.length-1)){
                    discapacidadsStr+=", "
                }
            }
            return discapacidadsStr;
        },
        authGrupoEtnico:(state)=>{
            return state.authPersona.grupo_etnico_id !== null ? state.authPersona.grupo_etnico.descripcion : 'No aplica';
        },
        //PERFIL LABORAL
        authAspiracionSalarial:(state)=>{
            if(state.authPerfilLaboral.aspiracion_salarial !== null && state.authPerfilLaboral.aspiracion_salarial !== ""){
                return state.authPerfilLaboral.aspiracion_salarial.descripcion
            }
            return '-';
        },
        authSituacionLaboral:(state)=>{
            return state.authPerfilLaboral.situacion_laboral_id !== null ? state.authPerfilLaboral.situacion_laboral.descripcion : '-';
        },
        authTipoLicenciaMoto:(state)=>{
            return state.authPerfilLaboral.tipo_licencia_conduccion_moto_id !== null ? state.authPerfilLaboral.tipo_licencia_conduccion_moto.descripcion : 'No aplica';
        },
        authTipoLicenciaCarro:(state)=>{
            return state.authPerfilLaboral.tipo_licencia_conduccion_carro_id !== null ? state.authPerfilLaboral.tipo_licencia_conduccion_carro.descripcion : 'No aplica';
        },
        authCargosInteres:(state)=>{
            let cargosStr = '';
            let cargos = state.authPerfilLaboral.cargos_interes;
            for(let i = 0; i<cargos.length; i++){
                cargosStr+=cargos[i].descripcion;
                if(i !== (cargos.length-1)){
                    cargosStr+=", "
                }
            }
            return cargosStr;
        },
    }
}

export default AuthModule;
